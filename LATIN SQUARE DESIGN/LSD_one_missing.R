## One missing treatments in LSD

lsddt<-read.csv(file.choose(),header=TRUE)
lsddt
attach(lsddt)
names(lsddt)
class(lsddt$rows)
class(lsddt$columns)
class(lsddt$treatments)
class(lsddt$values)
rows<-as.factor(lsddt$rows)
columns<-as.factor(lsddt$columns)
treatments<-as.factor(lsddt$treatments)
class(rows)
class(columns)
class(treatments)
levels(rows)
levels(columns)
levels(treatments)
r<-nlevels(rows)
c<-nlevels(columns)
t<-nlevels(treatments)
r
c
t
fmi<-match(NA,lsddt$values)
fmi
fmr<-rows[fmi]
fmr
fmc<-columns[fmi]
fmc
fmt<-treatments[fmi]
fmt
lsddt$values[fmi]=0
library(pivottabler)
table<-qpvt(lsddt,"rows","columns","sum(values)")
table
class(table)
dmat<-table$asDataMatrix()
class(dmat)
dmat
##for treatments
table1<-qpvt(lsddt,"rows","treatments","sum(values)")
table1
class(table1)
dmat1<-table$asDataMatrix()
class(dmat1)
dmat1
## calculation of missing values
g<-dmat["Total","Total"]
r1<-dmat[fmr,"Total"]
c1<-dmat["Total",fmc]
g
r1
c1
t1<-dmat1["Total",fmt]
t1

x=((t*(r1+c1+t1))-2*g)/((t-1)*(t-2))
x
lsddt$values[fmi]=x
lsddt

##Anova table for Lsd
lsdaov<-aov(lsddt$values~lsddt$rows+lsddt$columns+lsddt$treatments)
summary(lsdaov)
